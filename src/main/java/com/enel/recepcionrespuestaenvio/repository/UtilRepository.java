package com.enel.recepcionrespuestaenvio.repository;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

@Repository
public class UtilRepository {

	@PersistenceContext
	private EntityManager em;

	public Date obtenerFechaSistema() {
	    String sql = "select now()";
	    Query query = em.createNativeQuery(sql);
	    return (Date) query.getSingleResult();
	}

}
