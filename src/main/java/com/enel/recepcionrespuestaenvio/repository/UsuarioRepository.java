package com.enel.recepcionrespuestaenvio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionrespuestaenvio.entity.Usuario;
import com.enel.recepcionrespuestaenvio.util.Constantes;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	@Query(value = "SELECT id "
			+ "FROM " + Constantes.ESQUEMADOT + "usuario "
			+ "WHERE username = ?1", nativeQuery = true)
	Long obtenerIdUsuario(String user);
	
}
