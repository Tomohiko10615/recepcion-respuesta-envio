package com.enel.recepcionrespuestaenvio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionrespuestaenvio.entity.Parametros;
import com.enel.recepcionrespuestaenvio.util.Constantes;

@Repository
public interface ParametrosRepository extends JpaRepository<Parametros, Long> {

	@Query(value = "SELECT valor_num "
			+ "FROM " + Constantes.ESQUEMADOT + "com_parametros "
			+ "WHERE sistema = 'EORDER' "
			+ "AND entidad = 'ESTADO_TRANSFERENCIA' "
			+ "AND codigo = ?1", nativeQuery = true)
	Integer obtenerEstadoTransferencia(String codEst);

	@Query(value = "SELECT valor_alf "
			+ "FROM " + Constantes.ESQUEMADOT + "com_parametros "
			+ "WHERE sistema = 'EORDER' "
			+ "AND entidad = ?1 "
			+ "AND codigo = ?2 "
			+ "AND activo = 'S'", nativeQuery = true)
	String obtenerPath(String entidad, String key);

	@Query(value = "SELECT valor_alf "
			+ "FROM " + Constantes.ESQUEMADOT + "com_parametros "
			+ "WHERE sistema = 'EORDER' "
			+ "AND entidad = ?1 "
			+ "AND codigo = ?2", nativeQuery = true)
	String obtenerParametroNombre(String entidad, String codigo);

}
