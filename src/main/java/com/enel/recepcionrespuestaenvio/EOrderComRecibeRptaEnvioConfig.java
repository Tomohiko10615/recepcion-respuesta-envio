package com.enel.recepcionrespuestaenvio;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EOrderComRecibeRptaEnvioConfig {

	@Bean(name = "app")
	public EOrderComRecibeRptaEnvio registrarEOrderComRecibeRptaEnvio() {
		return new EOrderComRecibeRptaEnvio();
	}
}
