package com.enel.recepcionrespuestaenvio;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.enel.recepcionrespuestaenvio.service.ParametrosService;
import com.enel.recepcionrespuestaenvio.service.UsuarioService;
import com.enel.recepcionrespuestaenvio.util.Constantes;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class EOrderComRecibeRptaEnvio {

	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private ParametrosService parametrosService;
	
	private String user;
	private String sistema;
	private String odt;
	private Date fecha;
	
	private Long idUsuario;
	
	private Integer codEstEnvia;
	private Integer codEstEnver;
	
	private String rutaLegacysIn;
	private String rutaLegacysProc;
	private String rutaLgcGiOu;
	private String rutaLgcGpOu;
	private String rutaLgcNcOu;
	private String rutaLgcDcOu;
	private String rutaLgcFiOu;
	private String rutaGdiInSyn;
	private String rutaGdiInMun;
	private String rutaGdiInMan;
	
	private String[] paths = new String[10];
	
	private String[] entidades = {
			Constantes.PATH_LEGACY, Constantes.PATH_LEGACY,
			Constantes.PATH_LEGACY, Constantes.PATH_LEGACY,
			Constantes.PATH_LEGACY, Constantes.PATH_LEGACY,
			Constantes.PATH_LEGACY, Constantes.PATH_GDI,
			Constantes.PATH_GDI, Constantes.PATH_GDI};
	
	private String[] keys = {
			"LGC_W_IN", "LGC_PROCES", "LGC_GI_OU", "LGC_GP_OU", "LGC_NC_OU",
			"LGC_DC_OU", "LGC_FI_OU", "GDI_IN_SYN", "GDI_IN_MUN", "GDI_IN_MAN"};
	
	private String nombreArchivo;
	private String pathGdiIn;
	
	private String nomArchivoGestImp;
	private String nomArchivoGestPerd;
	private String nomArchivoConex;
	private String nomArchivoDesCom;
	private String nomArchivoFiscal;
	
	private File fArchivoErrGestImp;
	private File fArchivoErrGestPerd;
	private File fArchivoErrConex;
	private File fArchivoErrDesCom;
	private File fArchivoErrFiscal;

	private FileWriter fArchivoErrGestImpWriter;
	private FileWriter fArchivoErrGestPerdWriter;
	private FileWriter fArchivoErrConexWriter;
	private FileWriter fArchivoErrDesComWriter;
	private FileWriter fArchivoErrFiscalWriter;
	
	public boolean obtenerIdUsuario() {
		idUsuario = usuarioService.obtenerIdUsuario(user);
		if (idUsuario == null) {
			log.error("Error: El usuario No Existe {}", user);
			return false;
		}
		log.info("Se obtuvo la id de usuario: {}", idUsuario);
		return true;
	}

	public boolean obtenerEstadosTransferencia() {
		codEstEnvia = parametrosService.obtenerEstadoTransferencia("ENVIA");
		
		if (codEstEnvia == null) {
			log.error("Error: No se encuentra configurado el Estados de Transferencia [ENVIA]");
			return false;
		}
		
		codEstEnver = parametrosService.obtenerEstadoTransferencia("ENVER");
		
		if (codEstEnver == null) {
			log.error("Error: No se encuentra configurado el Estados de Transferencia [ENVER]");
			return false;
		}
		
		log.info("Se obtuvo el estado de transferencia {}", codEstEnvia);
		log.info("Se obtuvo el estado de transferencia {}", codEstEnver);
		
		return true;
	}

	public boolean obtenerPaths() {
		for (int i = 0; i < 10; i++) {
			paths[i] = parametrosService.obtenerPath(entidades[i], keys[i]);
			if (paths[i] == null) {
				log.error("Error al obtener ruta de archivos {}", keys[i]);
			}
			log.info("Se obtuvo la ruta: {}", paths[i]);
		}
		rutaLegacysIn = paths[0];
		rutaLegacysProc = paths[1];
		rutaLgcGiOu = paths[2];
		rutaLgcGpOu = paths[3];
		rutaLgcNcOu = paths[4];
		rutaLgcDcOu = paths[5];
		rutaLgcFiOu = paths[6];
		rutaGdiInSyn = paths[7];
		rutaGdiInMun = paths[8];
		rutaGdiInMan = paths[9];
		
		log.info("Se han obtenido todas las rutas de archivos con éxito.");
		return true;
	}

	public boolean armarNombreArchivo() {
		nombreArchivo = parametrosService.armarNombreArchivo(sistema);
		if (nombreArchivo == null) {
			log.error("Error: No se pudo generar el nombre del archivo de respuesta de envío {}", sistema);
			return false;
		}
		
		log.info("Se armó el nombre del archivo: {}", nombreArchivo);
		return true;
	}

	public boolean abrirArchivos(String sistema) {
		
		/* Para windows
		nomArchivoGestImp = "c:/" + nomArchivoGestImp;
		nomArchivoGestPerd = "c:/" + nomArchivoGestPerd;
		nomArchivoConex = "c:/" + nomArchivoConex;
		nomArchivoDesCom = "c:/" + nomArchivoDesCom;
		nomArchivoFiscal = "c:/" + nomArchivoFiscal;
		*/
		
		switch (sistema) {
		case ("EDESYN"):
			fArchivoErrGestImp = new File(nomArchivoGestImp);
			try {
				fArchivoErrGestImpWriter = new FileWriter(fArchivoErrGestImp, false);
			} catch (IOException e) {
				e.printStackTrace();
				log.error(Constantes.MSJ_ERROR_ARCHIVO_ERRORES, nomArchivoGestImp);
				return false;
			}
			
			log.info(nomArchivoGestImp);
			
			fArchivoErrGestPerd = new File(nomArchivoGestPerd);
			try {
				fArchivoErrGestPerdWriter = new FileWriter(fArchivoErrGestPerd, false);
			} catch (IOException e) {
				e.printStackTrace();
				log.error(Constantes.MSJ_ERROR_ARCHIVO_ERRORES, nomArchivoGestPerd);
				return false;
			}
			
			log.info(nomArchivoGestPerd);
			
			fArchivoErrConex = new File(nomArchivoConex);
			try {
				fArchivoErrConexWriter = new FileWriter(fArchivoErrConex, false);
			} catch (IOException e) {
				e.printStackTrace();
				log.error(Constantes.MSJ_ERROR_ARCHIVO_ERRORES, nomArchivoConex);
				return false;
			}
			
			log.info(nomArchivoConex);
			
			break;
		case ("EDEMUN"):
			fArchivoErrDesCom = new File(nomArchivoDesCom);
			try {
				fArchivoErrDesComWriter = new FileWriter(fArchivoErrDesCom, false);
			} catch (IOException e) {
				e.printStackTrace();
				log.error(Constantes.MSJ_ERROR_ARCHIVO_ERRORES, nomArchivoDesCom);
				return false;
			}
			
			log.info(nomArchivoDesCom);
		
			break;
		
		case ("EDEMAN"):
			fArchivoErrFiscal = new File(nomArchivoFiscal);
			try {
				fArchivoErrFiscalWriter = new FileWriter(fArchivoErrFiscal, false);
			} catch (IOException e) {
				e.printStackTrace();
				log.error(Constantes.MSJ_ERROR_ARCHIVO_ERRORES, nomArchivoFiscal);
				return false;
			}
			
			log.info(nomArchivoFiscal);
			
			fArchivoErrGestImp = new File(nomArchivoGestImp);
			try {
				fArchivoErrGestImpWriter = new FileWriter(fArchivoErrGestImp, false);
			} catch (IOException e) {
				e.printStackTrace();
				log.error(Constantes.MSJ_ERROR_ARCHIVO_ERRORES, nomArchivoGestImp);
				return false;
			}
			
			log.info(nomArchivoGestImp);
		
			break;

		default:
			break;
		}

		return true;
	}
	
	
}
