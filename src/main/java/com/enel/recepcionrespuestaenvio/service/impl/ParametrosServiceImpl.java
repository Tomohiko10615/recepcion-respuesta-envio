package com.enel.recepcionrespuestaenvio.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionrespuestaenvio.repository.ParametrosRepository;
import com.enel.recepcionrespuestaenvio.service.ParametrosService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ParametrosServiceImpl implements ParametrosService {

	@Autowired
	private ParametrosRepository parametrosRepository;
	
	@Override
	public Integer obtenerEstadoTransferencia(String codEst) {
		return parametrosRepository.obtenerEstadoTransferencia(codEst);
	}

	@Override
	public String obtenerPath(String entidad, String key) {
		return parametrosRepository.obtenerPath(entidad, key);
	}

	@Override
	public String armarNombreArchivo(String sistema) {
		String creaTdcR =
				parametrosRepository.obtenerParametroNombre(
						"COD_TRANSFER_GDI", "CREA_TDC_R");
		if (creaTdcR == null) {
			log.error("Error en bfnArmarNombreArchivo_INTO_vCREA_TDC_R");
			return null;
		}
		
		String creaTdc = parametrosRepository.obtenerParametroNombre(
				"COD_SERVICIO_GDI", "CREA_TDC");
		
		if (creaTdc == null) {
			log.error("Error en bfnArmarNombreArchivo_INTO_vCREA_TDC");
			return null;
		}
		
		return creaTdcR + "OUTPUT" + "EDE" + sistema + creaTdc;
	}

}
