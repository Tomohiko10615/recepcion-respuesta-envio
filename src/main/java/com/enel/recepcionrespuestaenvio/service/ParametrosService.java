package com.enel.recepcionrespuestaenvio.service;

public interface ParametrosService {

	Integer obtenerEstadoTransferencia(String codEst);

	String obtenerPath(String entidad, String key);

	String armarNombreArchivo(String sistema);

}
