package com.enel.recepcionrespuestaenvio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.enel.recepcionrespuestaenvio.service.UtilService;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class EOrderComRecibeRptaEnvioApplication implements CommandLineRunner {

	@Autowired
	private EOrderComRecibeRptaEnvio app;
	
	@Autowired
	private UtilService utilService;
	
	public static void main(String[] args) {
		SpringApplication.run(EOrderComRecibeRptaEnvioApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		log.info("Validando parámetros de entrada...");
		
		/* Valida la cantidad de parametros */
		if ( args.length != 4 ) {
			log.error("Uso de parámetros: <Usuario> <Sistema> <Nro. ODT>");
			System.exit(1);
		}
		
		log.info("Iniciando el programa...");
		
		app.setUser(args[1]);
		app.setSistema(args[2]);
		app.setOdt(args[3]);
		
		app.setFecha(utilService.obtenerFechaSistema());
		
		log.info("Obteniendo id de usuario...");
		
		if (!app.obtenerIdUsuario()) {
			System.exit(1);
		}
		
		log.info("Obteniendo estados de transferencia...");
		
		if (!app.obtenerEstadosTransferencia()) {
			System.exit(1);
		}
		
		log.info("Obteniendo paths...");
		
		if (!app.obtenerPaths()) {
			System.exit(1);
		}
		
		log.info("Armando nombre del archivo...");
		
		if (!app.armarNombreArchivo()) {
			System.exit(1);
		}
		
		log.info("Generando archivo de errores...");
		
		String extension = app.getOdt() + ".txt";
		
		switch (app.getSistema()) {
		case ("EDESYN"):
			app.setNomArchivoGestImp(
					app.getRutaLgcGiOu() +
					"GEST_IMP_ERRORES_RPTA_ENVIO_" +
					extension);
	
			app.setNomArchivoGestPerd(
					app.getRutaLgcGpOu() +
					"GEST_PERD_ERRORES_RPTA_ENVIO_" +
					extension);
			
			app.setNomArchivoConex(
					app.getRutaLgcNcOu() +
					"CONEX_ERRORES_RPTA_ENVIO_" +
					extension);
			
			app.setNombreArchivo(app.getRutaGdiInSyn() + app.getNombreArchivo());
			app.setPathGdiIn(app.getRutaGdiInSyn());
			
			break;
		case ("EDEMUN"):
			app.setNomArchivoDesCom(
					app.getRutaLgcDcOu() +
					"DES_COM_ERRORES_RPTA_ENVIO_" +
					extension);
			
			app.setNombreArchivo(app.getRutaGdiInMun() + app.getNombreArchivo());
			app.setPathGdiIn(app.getRutaGdiInMun());
			
			break;
		case ("EDEMAN"):
			app.setNomArchivoFiscal(
					app.getRutaLgcFiOu() +
					"FISCAL_ERRORES_RPTA_ENVIO_" +
					extension);
		
			app.setNomArchivoGestImp(
					app.getRutaLgcGiOu() +
					"GEST_IMP_ERRORES_RPTA_ENVIO_" +
					extension);
			
			app.setNombreArchivo(app.getRutaGdiInMan() + app.getNombreArchivo());
			app.setPathGdiIn(app.getRutaGdiInMan());
			
			break;
		default:
			break;
		}
		
		if (!app.abrirArchivos(app.getSistema())) {
			System.exit(1);
		}
		
		System.exit(0);
		
	}

}
