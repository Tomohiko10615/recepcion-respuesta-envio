package com.enel.recepcionrespuestaenvio.util;

public class Constantes {

	private Constantes() {
		super();
	}
	public static final String ESQUEMA = "SCHSCOM";
	public static final String ESQUEMADOT = "SCHSCOM.";
	public static final String MSJ_ERROR_RUTA = "No se encuentra el archivo con ruta: {}";
	public static final String PATH_LEGACY = "PATH_LEGACY";
	public static final String PATH_GDI = "PATH_GDI";
	public static final String MSJ_ERROR_ARCHIVO_ERRORES = "No se pudo generar el archivo de salida de Errores {}";
}
